﻿namespace SpaceInvaderPOO
{
    public interface IOvni : IObjetMobile
    {
        bool Collision(Missile _picMissile);
    }
}