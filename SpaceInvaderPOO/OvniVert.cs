﻿using System.Drawing;

namespace SpaceInvaderPOO
{
    public class OvniVert : Ovni
	{
        private int nbVies;

		public OvniVert(Form1 formulaireJeu, int _direction, int _left, int _top) : base(formulaireJeu) 
        {
            pic.Left = _left;
            pic.Top = _top;
            pic.Image = Image.FromFile("OvniVert.png");
            vitesse = 3;
            direction = _direction;
            nbVies = 1;
        }

        public override void Deplacer()
        {
            if (direction == 1)
                pic.Left += vitesse;
            else
                pic.Left -= vitesse;
            pic.Top += vitesse;
            if (GameOver())
                Data.gameOver = true;
        }

        public override bool Collision(Missile _picMissile)
        {
            if (_picMissile.Actif && pic.Bounds.IntersectsWith(_picMissile.Pic.Bounds))
            {
                if (nbVies <= 0)
                    return true;
                else
                {
                    _picMissile.Pic.Visible = false;
                    _picMissile.Actif = false;
                    nbVies--;
                }
            }
            return false;
        }
    }
}
