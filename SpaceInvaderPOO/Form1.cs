﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

// Dans cette version il y a plusieurs niveau disponibles, lorsqu'on réussi à atteindre tous les ovnis on passe au niveau suivant.
// Il y a également une bombe qui se promène et qui permet de faire exploser plusieurs ovnis en même temps.

namespace SpaceInvaderPOO
{
    public partial class Form1 : Form
    {
        int niveau = 1;             // Niveau FACILE au début, cettre variable servira à augmenter la vitesse pour les niveaux suivants.

        Tank tank;            // Déclaration de l'objet Tank, l'instanciation est faite dans le code à cause du Contols.Add (voir commentaire plus bas).
        Missile missile;      // Déclaration de l'objet Missile, l'instanciation est faite dans le code à cause du Contols.Add (voir commentaire plus bas).

        List<IOvni> listeOvnis = new List<IOvni>();     // Déclaration de la liste principale qui servira à conserver l'adresse mémoire de tous les ovnis qui se promènent dans le jeu.

        PictureBox picExplosion;        // Pas besoin d'être un objet mobile car ne fait qu'apparaitre et disparaitre, n'a rien en commun avec les autres objets.

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Instanciation du TANK et du MISSILE (ne peut pas être fait dans la déclaration car j'envois l'adresse mémoire du formulaire courant, il doit être "loadé" au complet).
            // J'envois l'adresse du formulaire courant juste pour AJOUTER LE CONTRÔLE directement de la classe, pour éviter d'avoir à dupliquer le Controls.Add().
            tank = new Tank(this);
            missile = new Missile(this);

            // L'image d'explosion peut être déclarée indépendamment, n'a pas de méthodes communes avec les objets qui bougent.  Elle ne fait qu'apparaître et disparaitre lorsqu'on frappe un ovni.
            picExplosion = new PictureBox();
            picExplosion.Image = Image.FromFile(@"Explosion.gif");
            picExplosion.Height = 40;
            picExplosion.Width = 40;
            picExplosion.SizeMode = PictureBoxSizeMode.StretchImage;
            picExplosion.Visible = false;
            Controls.Add(picExplosion);
            picExplosion.BringToFront();

            CommencerPartie();

        }

        private void CommencerPartie()
        {
            listeOvnis.Clear();
            // Instanciation des objets pour les OVNIS -  Évidemment on va conserver toutes les adresses des inscances dans une liste pour pouvoir les faire bouger.
            for (int x = 0; x < niveau; x++)
            {
                listeOvnis.Add(new OvniBleu(this));
                listeOvnis.Add(new OvniRose(this));
                listeOvnis.Add(new OvniJaune(this));
            }
            listeOvnis.Add(new OvniVert(this, 1, 0, 0));
            listeOvnis.Add(new OvniVert(this, 2, 700, 0));

            timerOvnis.Start();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
                missile.AfficherMissile(tank.Pic);
            else
            {
                if (e.KeyCode == Keys.Right)
                {
                    tank.Direction = 1;
                    tank.Deplacer();
                }
                if (e.KeyCode == Keys.Left)
                {
                    tank.Direction = 2;
                    tank.Deplacer();
                }
            }
        }

        private void timerOvnis_Tick(object sender, EventArgs e)
        {

            missile.Deplacer();
            for (int x = 0; x < listeOvnis.Count(); x++)
            {
                listeOvnis[x].Deplacer();
                if (Data.gameOver)
                {
                    timerOvnis.Stop();
                    MessageBox.Show("GAME OVER! Vous avez lamentablement échoué votre mission");
                    Application.Exit();
                }

                if (listeOvnis[x].Collision(missile))
                {
                    // On cache le missible après l'explostion car il a terminé sa course, avec sa méthode Déplacer() il va arrêter de bouger par le fait même.
                    missile.Pic.Visible = false;
                    missile.Actif = false;

                    // Affichage de l'image d'explosion à la bonne position  (position de l'ovnis abattue).
                    picExplosion.Left = listeOvnis[x].Pic.Left;
                    picExplosion.Top = listeOvnis[x].Pic.Top;
                    picExplosion.Visible = true;

                    // Ce timer permettra d'arrêter l'explosion après un délais qui est fixé au démarrage du programme et toujours constant.
                    timerExplosion.Start();

                    listeOvnis[x].Pic.Dispose();              // On efface l'image de l'ovnis du formulaire.
                    listeOvnis.RemoveAt(x);     // Étant donné que dans la méthode Abbattre l'image est effacé de la mémoire (Dispose), on efface également l'élément de la liste car plus besoin de le faire bouger inutilement.
                    break;
                }
            }

            // Étant donné qu'on efface l'élément abattue de la liste, si on se rend compte que la liste est vide c'est qu'on a réussi notre mission.
            if (listeOvnis.Count() == 0)
            {
                timerOvnis.Stop();
                MessageBox.Show("Bravo vous avez sauvé la planète de cette attaque surnoise, vous passez au niveau suivant!!");
                niveau++;
                CommencerPartie();
            }
        }

        // Seulement pour masquer l'image d'explosion après un court délais, pour éviter que l'image reste là jusqu'à la prochaine explosion.
        private void timerExplosion_Tick(object sender, EventArgs e)
        {
            picExplosion.Visible = false;
            timerExplosion.Stop();
        }

    }
}
