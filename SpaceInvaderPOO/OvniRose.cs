﻿using System.Drawing;


namespace SpaceInvaderPOO
{
    // Ces ovnis se déplacent seulement VERS LE BAS.
    public class OvniRose : Ovni
    {
        #region Constructeurs
        public OvniRose(Form1 formulaireJeu) : base(formulaireJeu) 
        {
            pic.Left = Data.nbAleatoire.Next(0, 700);
            pic.Top = Data.nbAleatoire.Next(15, 50);
            pic.Image = Image.FromFile("OvniRose.png");
            vitesse = 5;
            direction = 3;
        }
        #endregion

        #region Méthodes
        public override void Deplacer()
        {
            pic.Top += vitesse;
            if (GameOver())
                Data.gameOver = true;
        }

        public override bool Collision(Missile _picMissile)
        {
            return  _picMissile.Actif && pic.Bounds.IntersectsWith(_picMissile.Pic.Bounds);
        }
        #endregion
    }
}
