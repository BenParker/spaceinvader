﻿using System.Windows.Forms;

namespace SpaceInvaderPOO
{
    public abstract class ObjetMobile : IObjetMobile
    {
        #region Variables
        protected PictureBox pic;
        protected int direction;        // 1- GD    2- DG   3- HB   4- BH
        protected int vitesse;          // Vitesse variable car le missile est plus rapide que les Ovnis.  Certains ovnis vont plus rapidement que d'autre.
                                        // Plus ce nombre est grand plus ça va vite car c'est le saut que fait l'objet à chaque tick du timer.
        #endregion

        #region Propriétés
        public PictureBox Pic { get { return pic; } }
        public int Direction { get { return direction; } set { direction = value; } }
        public bool Actif { get; set; }
        #endregion

        #region Constructeurs
        public ObjetMobile(Form1 formulaireJeu)
        {
            // Création de l'image dans le constructeur avec toute ces valeurs par défaut.
            pic = new PictureBox();
            pic.SizeMode = PictureBoxSizeMode.StretchImage;
            formulaireJeu.Controls.Add(pic);           // Permet d'ajouter le contrôle dans le bon formulaire.
        }
        #endregion

        #region Méthodes
        public abstract void Deplacer();
        #endregion
    }
}
