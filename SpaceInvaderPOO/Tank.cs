﻿using System.Drawing;

namespace SpaceInvaderPOO
{
    public class Tank : ObjetMobile
    {
        #region Constructeurs
        public Tank(Form1 formulaireJeu) : base(formulaireJeu) 
        {
            pic.Width = 75;
            pic.Height = 30;
            pic.Left = 350;
            pic.Top = 475;
            pic.Image = Image.FromFile("Tank.png");
            vitesse = 35;
        }
        #endregion

        #region Méthodes
        public override void Deplacer()
        {
            // Le IF est important car si le tank est situé à l'extrème gauche ou droite de la fenêtre IL NE DOIT PAS BOUGER.
            // On le déplace donc juste si il est situé entre les coordonnées 1 et 699 du .Left
            if (direction == 1 && pic.Left < 700)
                pic.Left += vitesse;
            if (direction == 2 && pic.Left > 0)
                pic.Left -= vitesse;
        }
        #endregion
    }
}
