﻿namespace SpaceInvaderPOO
{
   partial class Form1
   {
      /// <summary>
      /// Variable nécessaire au concepteur.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Nettoyage des ressources utilisées.
      /// </summary>
      /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Code généré par le Concepteur Windows Form

      /// <summary>
      /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
      /// le contenu de cette méthode avec l'éditeur de code.
      /// </summary>
      private void InitializeComponent()
      {
			this.components = new System.ComponentModel.Container();
			this.timerExplosion = new System.Windows.Forms.Timer(this.components);
			this.timerOvnis = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// timerExplosion
			// 
			this.timerExplosion.Interval = 500;
			this.timerExplosion.Tick += new System.EventHandler(this.timerExplosion_Tick);
			// 
			// timerOvnis
			// 
			this.timerOvnis.Tick += new System.EventHandler(this.timerOvnis_Tick);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.WindowText;
			this.ClientSize = new System.Drawing.Size(795, 510);
			this.Name = "Form1";
			this.Text = "Space Invaders POO - V 1.0";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
			this.ResumeLayout(false);

      }

      #endregion
        private System.Windows.Forms.Timer timerExplosion;
        private System.Windows.Forms.Timer timerOvnis;
    }
}

