﻿using System.Windows.Forms;
using System.Drawing;

namespace SpaceInvaderPOO
{
    public class Missile : ObjetMobile
    {
        #region Constructeurs
        public Missile(Form1 formulaireJeu) : base(formulaireJeu)
        {
            pic.Width = 12;
            pic.Height = 25;
            pic.Left = 0;
            pic.Top = 0;
            pic.Image = Image.FromFile("Missile.png");
            vitesse = 30;
            direction = 4;
            pic.Visible = false;
            Actif = false;
        }
        #endregion

        #region Méthodes
        public void AfficherMissile(PictureBox picTemp)
        {
            pic.Visible = true;                            // En rendant le missile VISIBLE, il sera déplacer dans le timer - Voir méthodes suivante Deplacer()
            Actif = true;
            pic.Left = picTemp.Left + 32;
            pic.Top = picTemp.Top - 30;
        }

        public override void Deplacer()
        {
            if (pic.Visible)                               // Si on voit le missile on le fait bouger, sinon ça ne sert à rien car il n'est pas en action.
            {
                pic.Top -= vitesse;
                if (pic.Left < 0)                          // Cette condition sert à éviter de faire monter indéfiniment le missile lorsqu'il arrive en haut de la fenêtre.
                {
                    pic.Visible = false;
                    Actif = false;
                }
            }
        }
        #endregion
    }
}
