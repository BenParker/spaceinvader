﻿using System.Windows.Forms;

namespace SpaceInvaderPOO
{
    public interface IObjetMobile
    {
        bool Actif { get; set; }
        int Direction { get; set; }
        PictureBox Pic { get; }

        void Deplacer();
    }
}