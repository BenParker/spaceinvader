﻿namespace SpaceInvaderPOO
{
    public abstract class Ovni : ObjetMobile, IOvni
    {
        #region Variables

        #endregion

        #region Propriété

        #endregion


        #region Constructeurs
        public Ovni(Form1 formulaireJeu) : base(formulaireJeu)
        {
            pic.Width = 35;
            pic.Height = 35;
            direction = Data.nbAleatoire.Next(1, 3);
        }
        #endregion

        #region Méthodes

        public abstract bool Collision(Missile _picMissile);

        protected bool GameOver()
        {
            return (pic.Top > 460);
        }
        #endregion
    }
}
