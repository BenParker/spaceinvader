﻿using System.Drawing;


namespace SpaceInvaderPOO
{
    public class OvniJaune : Ovni
    {
        private int nbVies;

        #region Constructeurs
        public OvniJaune(Form1 formulaireJeu) : base(formulaireJeu)
        {
            pic.Left = Data.nbAleatoire.Next(0, 700);
            pic.Top = Data.nbAleatoire.Next(10, 50);
            pic.Image = Image.FromFile("OvniJaune.png");
            vitesse = 5;
            direction = Data.nbAleatoire.Next(1, 3);
            nbVies = 2;
        }
        #endregion

        #region Méthodes

        public override bool Collision(Missile _picMissile)
        {
            if (_picMissile.Actif && pic.Bounds.IntersectsWith(_picMissile.Pic.Bounds))
            {
                if (nbVies <= 0)
                    return true;
                else
                {
                    _picMissile.Pic.Visible = false;
                    _picMissile.Actif = false;
                    nbVies--;
                }
            }
            return false;
        }

        public override void Deplacer()
        {
            switch (direction)
            {
                case 1: pic.Left += vitesse; break;
                case 2: pic.Left -= vitesse; break;
            }

            // Si l'ovni atteint un des côtés de l'écran, il descend un peut vers le bas et change de direction.
            if (pic.Left < 0 || pic.Left > 750)
            {
                pic.Top += 35;
                if (direction == 1)
                    direction = 2;
                else
                    direction = 1;
            }

            if (GameOver())
                Data.gameOver = true;
        }

        #endregion

    }
}
