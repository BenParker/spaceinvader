﻿using System.Drawing;

namespace SpaceInvaderPOO
{
    // Ces ovnis se déplacenet gauche/droite mais quand ils arrivent à une extrémité ils descendent un peu.
    public class OvniBleu : Ovni
    {
        #region Constructeurs
        public OvniBleu(Form1 formulaireJeu) : base(formulaireJeu) 
        {
            pic.Left = Data.nbAleatoire.Next(0, 700);
            pic.Top = Data.nbAleatoire.Next(100, 300);
            pic.Image = Image.FromFile("OvniBleu.png");
            vitesse = 10;
        }
        #endregion

        #region Méthodes

        public override bool Collision(Missile _picMissile)
        {
            if (_picMissile.Actif && pic.Bounds.IntersectsWith(_picMissile.Pic.Bounds))
            {
                if (direction == 3)
                {
                    _picMissile.Actif = false;
                    _picMissile.Pic.Visible = false;
                    return true;
                }
                else
                    direction = 3;
            }
            return false;
        }

        public override void Deplacer()
        {
            switch (direction)
            {
                case 1: pic.Left += vitesse; break;
                case 2: pic.Left -= vitesse; break;
                case 3: pic.Top += vitesse * 3; break;
            }

            // La particularité des ovnis bleus c'est que lorsqu'il arrive au bout de la fenêtre à gauche ou à droite ils DESCENDENT un peu et tourne de bord (changement de direction).
            // Donc après le déplacement on vérifie si on a atteint une de ces extrémité et si c'est le cas on DESCEND l'objet un vers le bas (.Top) et ON CHANGE la direction.
            if (pic.Left < 0 || pic.Left > 750)
            {
                pic.Top += 40;
                if (direction == 1)
                    direction = 2;
                else
                    direction = 1;
            }

            if (GameOver())
                Data.gameOver = true;

        }
        #endregion
    }
}
