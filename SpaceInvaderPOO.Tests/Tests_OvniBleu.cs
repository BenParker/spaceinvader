﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SpaceInvaderPOO.Tests
{
    [TestClass]
    public class Tests_OvniBleu
    {
        [TestMethod]
        public void Test_Collision_LaCollisionEstDetecteALaDeuxiemeFois()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniBleu ovniBleu = new OvniBleu(form1);
            Missile missile = new Missile(form1);
            missile.Actif = true;

            // Collision
            ovniBleu.Pic.Top = 0;
            ovniBleu.Pic.Left = 0;

            #endregion

            bool estEnCollision1 = ovniBleu.Collision(missile);
            bool estEnCollision2 = ovniBleu.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision1);
            Assert.IsTrue(estEnCollision2);

            #endregion

        }
    }
}
