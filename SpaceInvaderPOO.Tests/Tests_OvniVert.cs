﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SpaceInvaderPOO.Tests
{
    [TestClass]
    public class Tests_OvniVert
    {
        [TestMethod]
        public void Test_Collision_LaCollisionEstDetecteALADeuxiemeFois()
        {
            #region Initialisations

            // Instancier
            Form1 form1 = new Form1();
            OvniVert ovniVert = new OvniVert(form1, 1, 0, 0);
            Missile missile = new Missile(form1);
            missile.Actif = true;

            #endregion

            bool estEnCollision1 = ovniVert.Collision(missile);
            missile.Actif = true;
            missile.Pic.Visible = true;

            bool estEnCollision2 = ovniVert.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision1);
            Assert.IsTrue(estEnCollision2);

            #endregion

        }
    }
}
