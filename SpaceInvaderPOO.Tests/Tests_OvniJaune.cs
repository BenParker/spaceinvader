﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SpaceInvaderPOO.Tests
{
    [TestClass]
    public class Tests_OvniJaune
    {
        [TestMethod]
        public void Test_Collision_LaCollisionEstDetecteALaTroisiemeFois()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniJaune ovniJaune = new OvniJaune(form1);
            Missile missile = new Missile(form1);
            missile.Actif = true;

            // Collision
            ovniJaune.Pic.Top = 0;
            ovniJaune.Pic.Left = 0;

            #endregion

            bool estEnCollision1 = ovniJaune.Collision(missile);
            missile.Actif = true; missile.Pic.Visible = true;

            bool estEnCollision2 = ovniJaune.Collision(missile);
            missile.Actif = true; missile.Pic.Visible = true;

            bool estEnCollision3 = ovniJaune.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision1);
            Assert.IsFalse(estEnCollision2);
            Assert.IsTrue(estEnCollision3);

            #endregion
        }

        [TestMethod]
        public void Test_Collision_SiMissilleInactifEtCollisionAlorsRetourneFaux()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniJaune ovniJaune = new OvniJaune(form1);
            Missile missile = new Missile(form1);
            missile.Actif = false;

            // Collision
            ovniJaune.Pic.Top = 0;
            ovniJaune.Pic.Left = 0;

            #endregion

            bool estEnCollision = ovniJaune.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision);

            #endregion
        }

        [TestMethod]
        public void Test_Collision_SiMissilleActifEtPasDeCollisionAlorsRetourneFaux()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniJaune ovniJaune = new OvniJaune(form1);
            Missile missile = new Missile(form1);
            missile.Actif = true;

            // Collision
            ovniJaune.Pic.Top = 100;
            ovniJaune.Pic.Left = 100;

            #endregion

            bool estEnCollision = ovniJaune.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision);

            #endregion
        }

        [TestMethod]
        public void Test_Collision_SiMissilleInactifEtPasDeCollisionAlorsRetourneFaux()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniJaune ovniJaune = new OvniJaune(form1);
            Missile missile = new Missile(form1);
            missile.Actif = false;

            // Collision
            ovniJaune.Pic.Top = 100;
            ovniJaune.Pic.Left = 100;

            #endregion

            bool estEnCollision = ovniJaune.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision);

            #endregion
        }
    }
}
