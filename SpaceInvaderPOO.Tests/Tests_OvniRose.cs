﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SpaceInvaderPOO.Tests
{
    [TestClass]
    public class Tests_OvniRose
    {
        [TestMethod]
        public void Test_Collision_SiMissileActifEtCollisionAlorsRetourneVrai()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniRose ovniRose = new OvniRose(form1);
            Missile missile = new Missile(form1);
            missile.Actif = true;

            // Collision
            ovniRose.Pic.Top = 0;
            ovniRose.Pic.Left = 0;

            #endregion

            bool estEnCollision = ovniRose.Collision(missile);

            #region Assertions

            Assert.IsTrue(estEnCollision);

            #endregion
        }

        [TestMethod]
        public void Test_Collision_SiMissilleInactifEtCollisionAlorsRetourneFaux()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniRose ovniRose = new OvniRose(form1);
            Missile missile = new Missile(form1);
            missile.Actif = false;

            // Collision
            ovniRose.Pic.Top = 0;
            ovniRose.Pic.Left = 0;

            #endregion

            bool estEnCollision = ovniRose.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision);

            #endregion
        }

        [TestMethod]
        public void Test_Collision_SiMissilleActifEtPasDeCollisionAlorsRetourneFaux()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniRose ovniRose = new OvniRose(form1);
            Missile missile = new Missile(form1);
            missile.Actif = true;

            // Collision
            ovniRose.Pic.Top = 100;
            ovniRose.Pic.Left = 100;

            #endregion

            bool estEnCollision = ovniRose.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision);

            #endregion
        }

        [TestMethod]
        public void Test_Collision_SiMissilleInactifEtPasDeCollisionAlorsRetourneFaux()
        {
            #region Initialisation

            // Instancier
            Form1 form1 = new Form1();
            OvniRose ovniRose = new OvniRose(form1);
            Missile missile = new Missile(form1);
            missile.Actif = false;

            // Collision
            ovniRose.Pic.Top = 100;
            ovniRose.Pic.Left = 100;

            #endregion

            bool estEnCollision = ovniRose.Collision(missile);

            #region Assertions

            Assert.IsFalse(estEnCollision);

            #endregion
        }
    }
}
